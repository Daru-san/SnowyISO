# SnowyISO
My custom nixos install flake

## Why?
I've always found the minimal nixos install lacking in terms of features and thought it would be a great idea to be able to install using a custom environment.

It is mainly inspired by the Fedora Sway installer iso, which I loved. Being mininal but practical is the goal. 

Feel free to copy it if you like what you see

## WM
* Sway

## Terminal programs
* Ranger
* Neovim
* Git
* btop
* neofetch
* zsh
* tmux

## Gui
* Firefox
* Gparted
